const firebase = require ('./firebase.js');

var db = firebase.initializeApp.firestore();
var Trips = db.collection('Trips');
var Stops = db.collection('Stops');
var fs = require('fs');

function getStops () {
    fs.readFile('./jsons/stops.json', 'utf8', function (err, data) {
        if (err) throw err;
        obj = JSON.parse(data);
        obj.results.bindings.forEach(element => {
            var StoredData = {};
            var frags = element.uri.value.split('/');
            var frags2 = frags[7].split('-')
            var id =  frags2[0];
            StoredData.name = element.foaf_name.value;
            StoredData.geo_lat = element.geo_lat.value;
            StoredData.geo_long = element.geo_long.value;
            Stops.doc(id).set(StoredData);
    });
    });
    console.log("All trips have been saved");
}

function getTrips () {

fs.readFile('./jsons/laborales.json', 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);
    obj.results.bindings.forEach(element => {
        if (element.gtfs_direction.value == 0){
            var StoredData = {};
            var id = element.gtfs_headsign.value.substring(0,2)+'-'+element.gtfs_direction.value;
            StoredData.geo_long = element.geo_long.value;
            StoredData.geo_lat = element.geo_lat.value;
            StoredData.gtfs_stopSequence = element.gtfs_stopSequence.value;
            Trips.doc(id).set({gtfs_direction: element.gtfs_direction.value, gtfs_headsign: element.gtfs_headsign.value})
            Trips.doc(id).collection("Laborales").doc(element.gtfs_stopSequence.value).set(StoredData)
        }else{
            var StoredData1 = {};
            var id1 = element.gtfs_headsign.value.substring(0,2)+'-'+element.gtfs_direction.value;
            StoredData1.geo_long = element.geo_long.value;
            StoredData1.geo_lat = element.geo_lat.value;
            StoredData1.gtfs_stopSequence = element.gtfs_stopSequence.value;
            Trips.doc(id1).set({gtfs_direction: element.gtfs_direction.value, gtfs_headsign: element.gtfs_headsign.value})
            Trips.doc(id1).collection("Laborales").doc(element.gtfs_stopSequence.value).set(StoredData1);
        }
    });    
});


fs.readFile('./jsons/sabados.json', 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);
    obj.results.bindings.forEach(element => {
        if (element.gtfs_direction.value == 0){
            var StoredData = {};
            var id = element.gtfs_headsign.value.substring(0,2)+'-'+element.gtfs_direction.value;
            StoredData.geo_long = element.geo_long.value;
            StoredData.geo_lat = element.geo_lat.value;
            StoredData.gtfs_stopSequence = element.gtfs_stopSequence.value;
            Trips.doc(id).set({gtfs_direction: element.gtfs_direction.value, gtfs_headsign: element.gtfs_headsign.value})
            Trips.doc(id).collection("Sabados").doc(element.gtfs_stopSequence.value).set(StoredData)
        }else{
            var StoredData1 = {};
            var id1 = element.gtfs_headsign.value.substring(0,2)+'-'+element.gtfs_direction.value;
            StoredData1.geo_long = element.geo_long.value;
            StoredData1.geo_lat = element.geo_lat.value;
            StoredData1.gtfs_stopSequence = element.gtfs_stopSequence.value;
            Trips.doc(id1).set({gtfs_direction: element.gtfs_direction.value, gtfs_headsign: element.gtfs_headsign.value})
            Trips.doc(id1).collection("Sabados").doc(element.gtfs_stopSequence.value).set(StoredData1);
        }
     });
    });


    fs.readFile('./jsons/festivos.json', 'utf8', function (err, data) {
        if (err) throw err;
        obj = JSON.parse(data);
        obj.results.bindings.forEach(element => {
            if (element.gtfs_direction.value == 0){
                var StoredData = {};
                var id = element.gtfs_headsign.value.substring(0,2)+'-'+element.gtfs_direction.value;
                StoredData.geo_long = element.geo_long.value;
                StoredData.geo_lat = element.geo_lat.value;
                StoredData.gtfs_stopSequence = element.gtfs_stopSequence.value;
                Trips.doc(id).set({gtfs_direction: element.gtfs_direction.value, gtfs_headsign: element.gtfs_headsign.value})
                Trips.doc(id).collection("Festivos").doc(element.gtfs_stopSequence.value).set(StoredData)
            }else{
                var StoredData1 = {};
                var id1 = element.gtfs_headsign.value.substring(0,2)+'-'+element.gtfs_direction.value;
                StoredData1.geo_long = element.geo_long.value;
                StoredData1.geo_lat = element.geo_lat.value;
                StoredData1.gtfs_stopSequence = element.gtfs_stopSequence.value;
                Trips.doc(id1).set({gtfs_direction: element.gtfs_direction.value, gtfs_headsign: element.gtfs_headsign.value})
                Trips.doc(id1).collection("Festivos").doc(element.gtfs_stopSequence.value).set(StoredData1);
            }
        });
            
        });
    console.log("All trips have been saved");
}

module.exports = {
    getTrips, 
    getStops
}