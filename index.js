const express = require ('express');
const morgan = require ('morgan');
const app = express();
const refreshdata = require ('./refreshdata.js')
const parse = require('./parseJsons.js');
//settings
app.set('port', process.env.PORT || 3001);

//middlewares
app.use(morgan('dev'));


app.listen(app.get('port'), () =>{
    console.log('Server started on port '+app.get('port'));
    refreshdata.MakeUpdate();
    //parse.getTrips();
    //parse.getStops();
});