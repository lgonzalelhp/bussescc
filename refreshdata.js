const firebase = require ('./firebase.js');
const {SparqlClient, SPARQL} = require('sparql-client-2');

var db = firebase.initializeApp.firestore();
var Vehicles = db.collection('Vehicles');

const client = new SparqlClient('http://opendata.caceres.es/sparql/')
  .register({
    db: 'http://opendata.caceres.es/recurso/transporte/estadoFlotaAutobuses/',
    dbo: 'http://opendata.caceres.es/recurso/transporte/estadoFlotaAutobuses/',
  });
var intervalTime = 25; 
  
function fetchBuses() {
    return client
      .query(SPARQL`
             SELECT ?uri ?rdfs_comment ?gtfs_direction ?geo_long (group_concat(distinct ?om_llegadaAParada;separator="; ") as ?llegadaAParada)  ?om_lineaBus ?geo_lat ?time_inXSDDateTimeStamp ?datex_individualVehicleSpeed ?rdfs_label  WHERE { 
    ?uri a om:AutobusUrbano. 
    OPTIONAL  {?uri rdfs:comment ?rdfs_comment. }
    OPTIONAL  {?uri gtfs:direction ?gtfs_direction. }
    OPTIONAL  {?uri geo:long ?geo_long. }
    OPTIONAL  {?uri om:llegadaAParada ?om_llegadaAParada. }
    OPTIONAL  {?uri om:lineaBus ?om_lineaBus. }
    OPTIONAL  {?uri geo:lat ?geo_lat. }
    OPTIONAL  {?uri time:inXSDDateTimeStamp ?time_inXSDDateTimeStamp. }
    OPTIONAL  {?uri datex:individualVehicleSpeed ?datex_individualVehicleSpeed. }
    OPTIONAL  {?uri rdfs:label ?rdfs_label. }
  }`)
  .execute()
  .then(response => Promise.resolve(response.results.bindings));
}

function fetchNextStops (num, bus) {
    return client
      .query(SPARQL`
      select ?uri ?om_distanciaAParada ?gtfs_arrivalTime ?om_parada where{
        ?uri rdfs:label "Parada `+num+` del autobús `+bus+`".
        OPTIONAL  {?uri om:distanciaAParada ?om_distanciaAParada. }
        OPTIONAL  {?uri gtfs:arrivalTime ?gtfs_arrivalTime. }
         OPTIONAL {?uri om:parada ?om_parada. }
     }`)
  .execute()
  .then(response => Promise.resolve(response.results.bindings));
}

function ExtractAndStoreInfo(BusInfo) {
    StoreData = {};
    NextStops = [];
    var frags = BusInfo.uri.value.split('/');  
    StoreData.uri =BusInfo.uri.value;
    StoreData.rdfs_comment =BusInfo.rdfs_comment.value;
    StoreData.geo_long =BusInfo.geo_long.value;
    StoreData.geo_lat =BusInfo.geo_lat.value;
    StoreData.time_inXSDDateTimeStamp =BusInfo.time_inXSDDateTimeStamp.value;
    StoreData.datex_individualVehicleSpeed =BusInfo.datex_individualVehicleSpeed.value;
    var label = BusInfo.rdfs_label.value.split(' ');
    StoreData.rdfs_label = label[2];
    if (BusInfo.gtfs_direction != undefined)
        StoreData.gtfs_direction = BusInfo.gtfs_direction.value;
    if (BusInfo.om_lineaBus != undefined)
        StoreData.om_lineaBus = BusInfo.om_lineaBus.value.substring(BusInfo.om_lineaBus.value.length-2, BusInfo.om_lineaBus.value.length).toUpperCase();
        Vehicles.doc(frags[7]).set(StoreData);   
    
    if (BusInfo.llegadaAParada.value.length > 0){
        fetchNextStops(1,frags[7])
        .then((data) =>{
            Stop = {};
            if (data[0] != undefined && data[0].gtfs_arrivalTime != undefined && data[0].om_distanciaAParada != undefined && data[0].om_parada != undefined){
                Stop.gtfs_arrivalTime = data[0].gtfs_arrivalTime.value;
                Stop.om_distanciaAParada = data[0].om_distanciaAParada.value;
                var spl = data[0].om_parada.value.split('/');
                var spl2 = spl[7].split('-')
                Stop.om_parada = spl2[0];
            } 
            Vehicles.doc(frags[7]).collection('Stops').doc('First').set(Stop);
    })
    fetchNextStops(2,frags[7])
        .then((data) =>{
            Stop = {};
            if (data[0] != undefined && data[0].gtfs_arrivalTime != undefined && data[0].om_distanciaAParada != undefined && data[0].om_parada != undefined){
                Stop.gtfs_arrivalTime = data[0].gtfs_arrivalTime.value;
                Stop.om_distanciaAParada = data[0].om_distanciaAParada.value;
                var spl = data[0].om_parada.value.split('/');
                var spl2 = spl[7].split('-')
                Stop.om_parada = spl2[0];
            }
            Vehicles.doc(frags[7]).collection('Stops').doc('Second').set(Stop);
    })
    fetchNextStops(3,frags[7])
        .then((data) =>{ 
            Stop = {};
            if (data[0] != undefined && data[0].gtfs_arrivalTime != undefined && data[0].om_distanciaAParada != undefined && data[0].om_parada != undefined){
                Stop.gtfs_arrivalTime = data[0].gtfs_arrivalTime.value;
                Stop.om_distanciaAParada = data[0].om_distanciaAParada.value;
                var spl = data[0].om_parada.value.split('/');
                var spl2 = spl[7].split('-')
                Stop.om_parada = spl2[0];
            }
            Vehicles.doc(frags[7]).collection('Stops').doc('Third').set(Stop);
    })
    }    
}

function MakeUpdatecallableFunction () {
    fetchBuses()
    .then(data => {
        data.forEach(element => {
           ExtractAndStoreInfo(element);
        });
        console.log("AN UPDATE HAS BEEN DONE!!!");
        console.log("Waiting "+intervalTime+" secs for a new update....");
    }) 
}

function MakeUpdate() {
    MakeUpdatecallableFunction();
    setInterval(MakeUpdatecallableFunction, intervalTime*1000);
}

module.exports = {
    MakeUpdate
}